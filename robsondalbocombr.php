<?php
/**
 * Robson Dal Bó - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   RobsonDalBo
 * @author    Nextcom <contato@nextcom.digital>
 * @copyright 2019 Nextcom
 * @license   Proprietary https://nextcom.digital
 * @link      https://robsondalbo.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Robson Dal Bó - mu-plugin
 * Plugin URI:  https://robsondalbo.com.br
 * Description: Customizations for robsondalbo.com.br site
 * Version:     1.0.0
 * Author:      Nextcom
 * Author URI:  https://nextcom.digital/
 * Text Domain: robsondalbo
 * License:     Proprietary
 * License URI: https://nextcom.digital
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('robsondalbocombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_robsondalbocombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'robsondalbocombr_frontpage_hero_id',
                'title'         => __('Hero', 'robsondalbocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Background Color
        $cmb_hero->add_field(
            array(
                'name'       => __('Background Color', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
                        )
                    ),
                ),
            )
        );
        
        //Hero Backgrpund Image
        $cmb_hero->add_field(
            array(
                'name'        => __('Background Image', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix.'hero_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Profile Image
        $cmb_hero->add_field(
            array(
                'name'        => __('Profile Picture', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix.'hero_profile_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Title
        $cmb_hero->add_field(
            array(
                'name'       => __('Title', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_field(
            array(
                'name'       => __('Text', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Button Text
        $cmb_hero->add_field(
            array(
                'name'       => __('Button Text', 'robsondalbocombr'),
                'desc'       => '',
                'default'    => __('Call to Action', 'robsondalbocombr'),
                'id'         => $prefix.'hero_btn_text',
                'type'       => 'text_small',
            )
        );

        //Hero Button URL
        $cmb_hero->add_field(
            array(
                'name'       => __('Button URL', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_btn_url',
                'type'       => 'text',
            )
        );

        /**
         * Banner
         */
        $cmb_banner = new_cmb2_box(
            array(
                'id'            => 'robsondalbocombr_frontpage_banner_id',
                'title'         => __('Banner', 'robsondalbocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show Banner
        $cmb_banner->add_field(
            array(
                'name' => __('Show Banner', 'robsondalbocombr'),
                'desc' => __('Show banner on front-page', 'robsondalbocombr'),
                'id'   => $prefix . 'banner_show',
                'type' => 'checkbox',
                'default' => true,
            )
        );

        //Banner Background Color
        $cmb_banner->add_field(
            array(
                'name'       => __('Background Color', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'banner_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
                        )
                    ),
                ),
            )
        );
        
        //Banner Background Image
        $cmb_banner->add_field(
            array(
                'name'        => __('Background Image', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix . 'banner_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Banner Text
        $cmb_banner->add_field(
            array(
                'name'       => __('Text', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'banner_text',
                'type'       => 'textarea_code',
            )
        );

        /******
         * About Robson
         ******/
        $cmb_about = new_cmb2_box(
            array(
                'id'            => 'robsondalbocombr_frontpage_about_id',
                'title'         => __('About Robson', 'robsondalbocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //About Background Color
        $cmb_about->add_field(
            array(
                'name'       => __('Background Color', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
                        )
                    ),
                ),
            )
        );
        
        //About Background Image
        $cmb_about->add_field(
            array(
                'name'        => __('Background Image', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix . 'about_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //About Title
        $cmb_about->add_field(
            array(
                'name'       => __('Title', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_title',
                'type'       => 'text',
            )
        );

        //About Content
        $cmb_about->add_field(
            array(
                'name'       => __('Content', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_content',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Benefits
         ******/
        $cmb_benefits = new_cmb2_box(
            array(
                'id'            => 'robsondalbocombr_frontpage_benefits_id',
                'title'         => __('Benefits', 'robsondalbocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Benefits Background Color
        $cmb_benefits->add_field(
            array(
                'name'       => __('Background Color', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'benefits_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
                        )
                    ),
                ),
            )
        );
        
        //Benefits Background Image
        $cmb_benefits->add_field(
            array(
                'name'        => __('Background Image', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix . 'benefits_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Benefits Title
        $cmb_benefits->add_field(
            array(
                'name'       => __('Title', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'benefits_title',
                'type'       => 'text',
            )
        );

        //Benefits Content
        //$cmb_benefits->add_field(
        //    array(
        //        'name'       => __('Content', 'robsondalbocombr'),
        //        'desc'       => '',
        //        'id'         => $prefix . 'benefits_content',
        //        'type'       => 'textarea_code',
        //    )
        //);

        //Benefits Group
        $benefits_id = $cmb_benefits->add_field(
            array(
                'id'          => $prefix.'benefits_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'  =>__('Benefit {#}', 'robsondalbocombr'),
                    'add_button' =>__('Add Another Benefit', 'robsondalbocombr'),
                    'remove_button'=>__('Remove Benefit', 'robsondalbocombr'),
                    'sortable'     => true, // beta
                ),
            )
        );

        //Benefit Item Icon
        $cmb_benefits->add_group_field(
            $benefits_id,
            array(
                'name'    => __('Icon', 'robsondalbocombr'),
                'desc'    => "",
                'id'      => 'icon',
                'type'    => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add icon', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //  'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(150, 150)
            )
        );

        //Benefit Item Title
        $cmb_benefits->add_group_field(
            $benefits_id,
            array(
                'name' => __('Title', 'robsondalbocombr'),
                'id'   => 'title',
                'type' => 'text',
            )
        );

        //Benefit Item Description
        $cmb_benefits->add_group_field(
            $benefits_id,
            array(
            'name' => __('Description', 'robsondalbocombr'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            )
        );

        /******
         * About ActionCOACH
         ******/
        $cmb_action = new_cmb2_box(
            array(
                'id'            => 'robsondalbocombr_frontpage_action_id',
                'title'         => __('About ActionCOACH', 'robsondalbocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Action Background Color
        $cmb_action->add_field(
            array(
                'name'       => __('Background Color', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'action_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
                        )
                    ),
                ),
            )
        );
        
        //Action Background Image
        $cmb_benefits->add_field(
            array(
                'name'        => __('Background Image', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix . 'action_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Action Title
        $cmb_action->add_field(
            array(
                'name'       => __('Title', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'action_title',
                'type'       => 'text',
            )
        );

        //Action Content
        $cmb_action->add_field(
            array(
                'name'       => __('Content', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'action_content',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Form
         */
        $cmb_form = new_cmb2_box(
            array(
                'id'            => 'robsondalbocombr_frontpage_form_id',
                'title'         => __('Form', 'robsondalbocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Form Background Color
        $cmb_form->add_field(
            array(
                'name'       => __('Background Color', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
                        )
                    ),
                ),
            )
        );
        
        //Form Background Image
        $cmb_form->add_field(
            array(
                'name'        => __('Background Image', 'robsondalbocombr'),
                'description' => '',
                'id'          => $prefix . 'form_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Form Title
        $cmb_form->add_field(
            array(
                'name'       => __('Form Title', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_title',
                'type'       => 'textarea_code',
            )
        );

        //Form Text
        $cmb_form->add_field(
            array(
                'name'       => __('Form Text', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_text',
                'type'       => 'text',
            )
        );

        //Form Form Shortcode
        $cmb_form->add_field(
            array(
                'name'       => __('Form Shortcode', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'text',
            )
        );

        //Form Confirmation Title
        $cmb_form->add_field(
            array(
                'name'       => __('Form Confirmation Title', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_confirmation_title',
                'type'       => 'text',
            )
        );

        //Form Confirmation Text
        $cmb_form->add_field(
            array(
                'name'       => __('Form Confirmation Text', 'robsondalbocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_confirmation_text',
                'type'       => 'textarea_code',
            )
        );

        ///**
        // * Footer
        // */
        //$cmb_footer = new_cmb2_box(
        //    array(
        //        'id'            => 'robsondalbo_frontpage_footer_id',
        //        'title'         => __('Footer', 'robsondalbocombr'),
        //        'object_types'  => array('page'), // post type
        //        'show_on' => array('key' => 'slug', 'value' => 'front-page'),
        //        'context'       => 'normal',
        //        'priority'      => 'high',
        //        'show_names'    => true, // Show field names on the left
        //    )
        //);

        ////Footer Background Color
        //$cmb_footer->add_field(
        //    array(
        //        'name'       => __('Background Color', 'robsondalbocombr'),
        //        'desc'       => '',
        //        'id'         => $prefix . 'footer_bkg_color',
        //        'type'       => 'colorpicker',
        //        'default'    => '#ffffff',
        //        'attributes' => array(
        //            'data-colorpicker' => json_encode(
        //                array(
        //                    'palettes' => array( '#ffffff', '#000000', '#343f70', '#fb2130'),
        //                )
        //            ),
        //        ),
        //    )
        //);
        
        ////Footer Background Image
        //$cmb_footer->add_field(
        //    array(
        //        'name'        => __('Background Image', 'robsondalbocombr'),
        //        'description' => '',
        //        'id'          => $prefix . 'footer_bkg_image',
        //        'type'        => 'file',
        //        // Optional:
        //        'options' => array(
        //            'url' => false, // Hide the text input for the url
        //        ),
        //        'text'    => array(
        //            'add_upload_file_text' =>__('Add Image', 'robsondalbocombr'),
        //        ),
        //        // query_args are passed to wp.media's library query.
        //        'query_args' => array(
        //            'type' => array(
        //                //'image/gif',
        //                'image/jpeg',
        //                'image/png',
        //            ),
        //        ),
        //        'preview_size' => array(320, 180)
        //    )
        //);

        ////Footer Copyright
        //$cmb_footer->add_field(
        //    array(
        //        'name'       => __('Footer Text', 'robsondalbocombr'),
        //        'desc'       => '',
        //        'id'         => $prefix . 'footer_text',
        //        'type'       => 'text',
        //    )
        //);
    }
);
